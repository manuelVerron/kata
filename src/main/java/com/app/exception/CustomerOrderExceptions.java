package com.app.exception;

/**
 * Classe d'exception por la classe CustomerOrder
 * @author hx
 *
 */
public class CustomerOrderExceptions extends Exception{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8392920631151459630L;
	
	private String message;
	
		public CustomerOrderExceptions(String s){  
			super(s);  
			this.message = s;
		 }  
		
		@Override
		public String getMessage() {
			return this.message;
		}
}
