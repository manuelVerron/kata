package com.app;

/**
 * Interface pour les notifications email
 * @author hx
 *
 */
public interface EmailNotifier {
	
	void notifyMissingDrink(String drink);
}
