package com.app;

/**
 * Overview de la machine à café.
 * @author hx
 *
 */
public class CoffeeMachineOverview {
	
	private static CoffeeMachineOverview INSTANCE;
	
	private int coffeeCounter;
	
	private int teaCounter;
	
	private int chocolateCounter;
	
	private int orangeCounter;
	
	private double totalBenefits;

	/**
	 * Singleton
	 */
	private CoffeeMachineOverview() {
		
	}
	
	/**
	 * Retourne une instance unique
	 * @return L'instance de la classe
	 */
	public static CoffeeMachineOverview getInstance() {
		if(INSTANCE == null) {
			INSTANCE = new CoffeeMachineOverview();
		}
		
		return INSTANCE;
	}
	
	/**
	 * Permet de mettre à jour les compteurs et le bénéfice à chaque ajout de commande
	 * @param beverage La nouvelle boisson commandée
	 */
	public void addOrder(Beverage beverage) {
		switch (beverage) {
		case CHOCOLATE:
			chocolateCounter += 1;
			break;
		case COFFEE:
			coffeeCounter += 1;
			break;
		case ORANGE:
			orangeCounter += 1;
			break;
		case TEA:
			teaCounter += 1;
			break;
		}
		
		totalBenefits += beverage.getPrice();
	}

	/**
	 * Renvoi et affiche dans la console l'overview de la machine à café
	 * @return
	 */
	public String getOverview() {
		String message = String.format("Coffee Machine Overview (**VERY CONFIDENTIAL**)"
				+ "\n"
				+ "\n"
				+ "Chocolate : " + chocolateCounter
				+ "\n"
				+ "Coffee : " + coffeeCounter
				+ "\n"
				+ "Orange : " + orangeCounter
				+ "\n"
				+ "Tea : " + teaCounter
				+ "\n"
				+ "--------------"
				+ "\n"
				+ "Total benefits : %.2f euro", totalBenefits);
		
		return message;
	}
	
}
