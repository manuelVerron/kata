package com.app;

/**
 * Enum pour les différentes boissons
 * @author hx
 *
 */
public enum Beverage {
	
	COFFEE(.6),
	TEA(.4),
	CHOCOLATE(.5),
	ORANGE(.6);
	
	
	private final double price;
	
	/**
	 * 
	 * @param price Le prix de la boisson
	 */
	Beverage(double price) {
        this.price = price;
    }
	
    public double getPrice() { return price; }
}
