package com.app;

import com.app.exception.CustomerOrderExceptions;

/**
 * Représente les commandes client
 * @author hx
 *
 */
public class CustomerOrder {
    
	private Beverage beverage;
	
	private int numberOfSugar;
	
	private boolean stick;
	
	private double money;
	
	private boolean extraHot;
	
	private BeverageQuantityChecker beverageQuantityChecker;
	
	private EmailNotifier emailNotifier;

	/**
	 *  Constructeur de la commande
	 * @param beverage La boisson à faire
	 * @param numberOfSugar Le nombre de sucre. Si la valeur est négative est sera mise à 0 et si la valeur est supérieure à 2 elle sera mise à 2.
	 * @param money La monnaie donnée pour la boisson
	 * @throws CustomerOrderExceptions Levée si la boisson est null ou si la monnaie est négative
	 */
	public CustomerOrder(Beverage beverage, boolean extraHot, int numberOfSugar, double money) throws CustomerOrderExceptions{
		super();
		
		if(beverage == null) {
			throw new CustomerOrderExceptions("Beverage cannot be null");
		}
		
		if(money < 0) {
			throw new CustomerOrderExceptions("Money cannot be negative");
		}
		
		this.beverage = beverage;
		
		this.extraHot = extraHot;
		
		if(numberOfSugar <= 0) {
			this.numberOfSugar = 0;
		} else if(numberOfSugar >= 2) {
			this.numberOfSugar = 2;
		} else {
			this.numberOfSugar = numberOfSugar;
		}
		
		this.stick  = numberOfSugar == 0 ? false : true;
		
		this.setMoney(money);
		
		CoffeeMachineOverview.getInstance().addOrder(this.beverage);
	}
	
	/**
	 * Constructeur de la commande pour un jus d'orange. Peut-être utilisé pour les autres boisson mais
	 *  il n'y aura pas de sucre et elle ne sera pas extra hot.
	 * @param beverage La boisson à faire
	 * @param money La monnaie donnée pour la boisson
	 * @throws CustomerOrderExceptions Levée si la boisson est null ou si la monnaie est négative
	 */
	public CustomerOrder(Beverage beverage, double money) throws CustomerOrderExceptions{
		this(beverage, false, 0, money);
	}

	public Beverage getBeverage() {
		return beverage;
	}

	public void setBeverage(Beverage beverage) {
		this.beverage = beverage;
	}

	public int getNumberOfSugar() {
		return numberOfSugar;
	}

	public void setNumberOfSugar(int numberOfSugar) {
		this.numberOfSugar = numberOfSugar;
	}

	public boolean isStick() {
		return stick;
	}

	public void setStick(boolean stick) {
		this.stick = stick;
	}

	/**
	 * Envoi de la commande à la machine
	 * @return Le message transmis à la machine
	 */
	public String sendOrder() {
		
		if(this.money < this.beverage.getPrice()) {
			double missingAmount = this.beverage.getPrice() - this.money;
			try {
				return this.sendMessage(String.format("Please had %.2f euro in order to get your drink", missingAmount));
			} catch (CustomerOrderExceptions e) {
				e.printStackTrace();
			}
		}
		
		if(beverageQuantityChecker.isEmpty(beverage.name())) {
			try {
				emailNotifier.notifyMissingDrink(beverage.name());
				return this.sendMessage(String.format("Error : %s is empty. A notification has been sent.", beverage.name()));
			} catch (CustomerOrderExceptions e) {
				e.printStackTrace();
			}
		}
		
		StringBuilder message = new StringBuilder();
		
		switch (this.beverage) {
		case COFFEE:
			message.append("C");
			break;
		case TEA:
			message.append("T");
			break;
		case CHOCOLATE:
			message.append("H");
			break;
		case ORANGE:
			message.append("O");
			break;
		}
		
		message.append(this.isExtraHot() ? "h:" : ":");
		
		if(this.numberOfSugar == 0) {
			message.append(":");
		} else if(this.numberOfSugar == 1) {
			message.append("1:");
		} else {
			message.append("2:");
		}
		
		message.append(this.isStick() ? "0" : "");
		
		return message.toString();
	}
	
	/**
	 * For test purpose only
	 * @param beverageQuantityChecker mock
	 */
	public void setBeverageQuantityChecker(BeverageQuantityChecker beverageQuantityChecker) {
		this.beverageQuantityChecker = beverageQuantityChecker;
	}

	/**
	 * For test purpose only
	 * @param emailNotifier mock
	 */
	public void setEmailNotifier(EmailNotifier emailNotifier) {
		this.emailNotifier = emailNotifier;
	}
	
	/**
	 * Envoi du message à la machine
	 * @param msgToSend Le message à envoyer
	 * @return Le message transmis à la machine
	 * @throws CustomerOrderExceptions Levée si le message est null
	 */
	public String sendMessage(String msgToSend) throws CustomerOrderExceptions {
		if(msgToSend == null) {
			throw new CustomerOrderExceptions("Please insert a message");
		}
		
		return "M:" + msgToSend;
	}

	public double getMoney() {
		return money;
	}

	public void setMoney(double money) {
		this.money = money;
	}

	public boolean isExtraHot() {
		return extraHot;
	}

	public void setExtraHot(boolean extraHot) {
		this.extraHot = extraHot;
	}
	
}
