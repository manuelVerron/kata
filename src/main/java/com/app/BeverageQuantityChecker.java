package com.app;

/**
 * Interface pour la vérification des quantités des boissons.
 * @author hx
 *
 */
public interface BeverageQuantityChecker {

	boolean isEmpty(String drink);
}
