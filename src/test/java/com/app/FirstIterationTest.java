package com.app;

import static org.junit.Assert.assertFalse;
import static org.assertj.core.api.Assertions.*;

import org.junit.Test;

import com.app.exception.CustomerOrderExceptions;

/**
 * Classe de test pour la première itération
 */
public class FirstIterationTest 
{

//	@Test
//	public void orderWithNoSuGarTest() throws CustomerOrderExceptions{
//		//Given
//		CustomerOrder order;
//
//		//When
//		order = new CustomerOrder(Beverage.COFFEE, 0);
//
//		//Then
//		assertFalse(order.isStick());
//	}
//
//	@SuppressWarnings("unused")
//	@Test
//	public void orderWithNullBeverageTest(){
//		//Given
//		CustomerOrder order;
//		boolean error = false;
//
//		//When
//		try {
//			order = new CustomerOrder(null, -1);
//		} catch (CustomerOrderExceptions e) {
//			if(e.getMessage().equals("Beverage cannot be null")) {
//				error = true;
//			}
//		}
//
//		//Then
//		assertThat(error).isTrue();
//	}
//	
//	@Test
//	public void orderWithNegativeNumberOfSugarTest() throws CustomerOrderExceptions{
//		//Given
//		CustomerOrder order;
//
//		//When
//		order = new CustomerOrder(Beverage.COFFEE, -1);
//
//		//Then
//		assertThat(order.getNumberOfSugar()).isEqualTo(0);
//	}
//
//	@Test
//	public void orderWithNumberOfSugarMoreThanTwoTest() throws CustomerOrderExceptions{
//		//Given
//		CustomerOrder order;
//
//		//When
//		order = new CustomerOrder(Beverage.COFFEE, 4);
//
//		//Then
//		assertThat(order.getNumberOfSugar()).isEqualTo(2);
//	}
//
//	@Test
//	public void orderWithNumberOfSugarEqualsToOneTest() throws CustomerOrderExceptions{
//		//Given
//		CustomerOrder order;
//
//		//When
//		order = new CustomerOrder(Beverage.COFFEE, 1);
//
//		//Then
//		assertThat(order.getNumberOfSugar()).isEqualTo(1);
//	}
//
//	@Test
//	public void sendCoffeeOrderTest() throws CustomerOrderExceptions{
//		//Given
//		CustomerOrder order = new CustomerOrder(Beverage.COFFEE, 2);
//		
//		//When
//		String message = order.sendOrder();
//
//		//Then
//		assertThat(message).isEqualTo("C:2:0");
//	}
//
//	@Test
//	public void sendTeaOrderTest() throws CustomerOrderExceptions{
//		//Given
//		CustomerOrder order = new CustomerOrder(Beverage.TEA, 1);
//		
//		//When
//		String message = order.sendOrder();
//
//		//Then
//		assertThat(message).isEqualTo("T:1:0");
//	}
//
//	@Test
//	public void sendChocolateOrderTest() throws CustomerOrderExceptions{
//		//Given
//		CustomerOrder order = new CustomerOrder(Beverage.CHOCOLATE, 0);
//		
//		//When
//		String message = order.sendOrder();
//
//		//Then
//		assertThat(message).isEqualTo("H::");
//	}
//	
//	@Test
//	public void sendMessageTest() throws CustomerOrderExceptions{
//		//Given
//		CustomerOrder order = new CustomerOrder(Beverage.CHOCOLATE, 0);
//		String msgToSend = "My beautiful message";
//		
//		//When
//		String message = order.sendMessage(msgToSend);
//
//		//Then
//		assertThat(message).isEqualTo("M:"+msgToSend);
//	}
//	
//	@Test
//	public void sendMessageNullTest() throws CustomerOrderExceptions{
//		//Given
//		CustomerOrder order = new CustomerOrder(Beverage.CHOCOLATE, 0);
//		String msgToSend = null;
//		boolean error = false;
//		
//		//When
//		try {
//			order.sendMessage(msgToSend);
//		} catch(CustomerOrderExceptions e) {
//			if(e.getMessage().equals("Please insert a message")) {
//				error = true;
//			}
//		}
//
//		//Then
//		assertThat(error).isTrue();
//	}
}
