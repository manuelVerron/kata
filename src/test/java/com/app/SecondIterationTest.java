package com.app;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

import com.app.exception.CustomerOrderExceptions;

/**
 * Classe de test pour la seconde itération
 */
public class SecondIterationTest 
{

//	@Test
//	public void orderCorrectMoneyWithCoffeeTest() throws CustomerOrderExceptions{
//		//Given
//		CustomerOrder order = new CustomerOrder(Beverage.COFFEE, 0, 1);
//
//		//When
//		String message = order.sendOrder();
//
//		//Then
//		assertThat(message).isEqualTo("C::");
//	}
//	
//	@Test
//	public void orderNotEnoughMoneyWithCoffeeTest() throws CustomerOrderExceptions{
//		//Given
//		CustomerOrder order = new CustomerOrder(Beverage.COFFEE, 0, .1);
//
//		//When
//		String message = order.sendOrder();
//
//		//Then
//		assertThat(message).isEqualTo("M:Please had 0,50 euro in order to get your drink");
//	}
//	
//	@Test
//	public void orderCorrectMoneyWithTeaTest() throws CustomerOrderExceptions{
//		//Given
//		CustomerOrder order = new CustomerOrder(Beverage.TEA, 1, 1);
//
//		//When
//		String message = order.sendOrder();
//
//		//Then
//		assertThat(message).isEqualTo("T:1:0");
//	}
//	
//	@Test
//	public void orderNotEnoughMoneyWithTeaTest() throws CustomerOrderExceptions{
//		//Given
//		CustomerOrder order = new CustomerOrder(Beverage.TEA, 0, .1);
//
//		//When
//		String message = order.sendOrder();
//
//		//Then
//		assertThat(message).isEqualTo("M:Please had 0,30 euro in order to get your drink");
//	}
//	
//	@Test
//	public void orderCorrectMoneyWithChocolateTest() throws CustomerOrderExceptions{
//		//Given
//		CustomerOrder order = new CustomerOrder(Beverage.CHOCOLATE, 0, 1);
//
//		//When
//		String message = order.sendOrder();
//
//		//Then
//		assertThat(message).isEqualTo("H::");
//	}
//	
//	@Test
//	public void orderNotEnoughMoneyWithChocolateTest() throws CustomerOrderExceptions{
//		//Given
//		CustomerOrder order = new CustomerOrder(Beverage.CHOCOLATE, 0, .1);
//
//		//When
//		String message = order.sendOrder();
//
//		//Then
//		assertThat(message).isEqualTo("M:Please had 0,40 euro in order to get your drink");
//	}
//	
//	@SuppressWarnings("unused")
//	@Test
//	public void orderWithNegativeMoneyTest() throws CustomerOrderExceptions{
//		//Given
//		CustomerOrder order;
//		boolean error = false;
//
//		//When
//		try {
//			order = new CustomerOrder(Beverage.COFFEE, 0, -1);
//		} catch (CustomerOrderExceptions e) {
//			if(e.getMessage().equals("Money cannot be negative")) {
//				error = true;
//			}
//		}
//
//		//Then
//		assertThat(error).isTrue();
//	}
}
