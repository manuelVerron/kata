package com.app;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;

import com.app.exception.CustomerOrderExceptions;

/**
 * Classe de test pour la cinquième itération
 */
public class FifthIterationTest 
{
	BeverageQuantityChecker checkerMock;
	
	EmailNotifier notifierMock;
	
	@Before
	public void init() {
		checkerMock = mock(BeverageQuantityChecker.class);
		notifierMock = mock(EmailNotifier.class);
	}
	
	@Test
	public void coffeeMachineNotEmptyTest() throws CustomerOrderExceptions{
		//Given
		//Lorsque la méthode isEmpty sera appelée avec n'importe quelle boisson elle retournera false
		when(checkerMock.isEmpty(anyString())).thenReturn(false);
		
		CustomerOrder order = new CustomerOrder(Beverage.ORANGE, 1);
		//On set le mock
		order.setBeverageQuantityChecker(checkerMock);

		//When
		String result = order.sendOrder();

		//Then
		assertThat(result).isNotEqualTo("M:Error : ORANGE is empty. A notification has been sent.");
	}
	
	@Test
	public void coffeeMachineEmptyTest() throws CustomerOrderExceptions{
		//Given
		//Lorsque la méthode isEmpty sera appelée avec n'importe quelle boisson elle retournera true
		when(checkerMock.isEmpty(anyString())).thenReturn(true);
		
		CustomerOrder order = new CustomerOrder(Beverage.ORANGE, 1);
		//On set les mocks
		order.setBeverageQuantityChecker(checkerMock);
		order.setEmailNotifier(notifierMock);

		//When
		String result = order.sendOrder();

		//Then
		assertThat(result).isEqualTo("M:Error : ORANGE is empty. A notification has been sent.");
	}
	
	@Test
	public void coffeeMachineEmptyNotificationTest() throws CustomerOrderExceptions{
		//Given
		
		//Lorsque la méthode isEmpty sera appelée avec n'importe quelle boisson elle retournera true
		when(checkerMock.isEmpty("COFFEE")).thenReturn(true);
		
		CustomerOrder order = new CustomerOrder(Beverage.COFFEE, 1);
		
		//On set les mocks
		order.setBeverageQuantityChecker(checkerMock);
		order.setEmailNotifier(notifierMock);

		//When
		String result = order.sendOrder();

		//Then
		verify(notifierMock, times(1)).notifyMissingDrink(anyString());
		assertThat(result).isEqualTo("M:Error : COFFEE is empty. A notification has been sent.");
	}
}
