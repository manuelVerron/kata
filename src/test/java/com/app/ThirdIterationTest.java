package com.app;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

import com.app.exception.CustomerOrderExceptions;

/**
 * Classe de test pour la troisième itération
 */
public class ThirdIterationTest 
{

//	@Test
//	public void orderOrangeWithCorrectMoneyTest() throws CustomerOrderExceptions{
//		//Given
//		CustomerOrder order = new CustomerOrder(Beverage.ORANGE, 1);
//
//		//When
//		String message = order.sendOrder();
//
//		//Then
//		assertThat(message).isEqualTo("O::");
//	}
//
//	@Test
//	public void orderOrangeWithNegativeMoneyTest() throws CustomerOrderExceptions{
//		//Given
//		boolean error = false;
//
//		//When
//		try {
//			new CustomerOrder(Beverage.ORANGE, -1);
//		} catch (CustomerOrderExceptions e) {
//			if(e.getMessage().equals("Money cannot be negative")) {
//				error = true;
//			}
//		}
//
//		//Then
//		assertThat(error).isTrue();
//	}
//
//	@Test
//	public void orderNormalCoffeeTest() throws CustomerOrderExceptions{
//		//Given
//		CustomerOrder order = new CustomerOrder(Beverage.COFFEE, false, 0, 1);
//
//		//When
//		String message = order.sendOrder();
//
//		//Then
//		assertThat(message).isEqualTo("C::");
//	}
//
//	@Test
//	public void orderExtraHotCoffeeTest() throws CustomerOrderExceptions{
//		//Given
//		CustomerOrder order = new CustomerOrder(Beverage.COFFEE, true, 0, 1);
//
//		//When
//		String message = order.sendOrder();
//
//		//Then
//		assertThat(message).isEqualTo("Ch::");
//	}
//	
//	@Test
//	public void orderNormalTeaTest() throws CustomerOrderExceptions{
//		//Given
//		CustomerOrder order = new CustomerOrder(Beverage.TEA, false, 2, 1);
//
//		//When
//		String message = order.sendOrder();
//
//		//Then
//		assertThat(message).isEqualTo("T:2:0");
//	}
//
//	@Test
//	public void orderExtraHotTeaTest() throws CustomerOrderExceptions{
//		//Given
//		CustomerOrder order = new CustomerOrder(Beverage.TEA, true, 2, 1);
//
//		//When
//		String message = order.sendOrder();
//
//		//Then
//		assertThat(message).isEqualTo("Th:2:0");
//	}
//	
//	@Test
//	public void orderNormalChocolateTest() throws CustomerOrderExceptions{
//		//Given
//		CustomerOrder order = new CustomerOrder(Beverage.CHOCOLATE, false, 1, 1);
//
//		//When
//		String message = order.sendOrder();
//
//		//Then
//		assertThat(message).isEqualTo("H:1:0");
//	}
//
//	@Test
//	public void orderExtraHotChocolateTest() throws CustomerOrderExceptions{
//		//Given
//		CustomerOrder order = new CustomerOrder(Beverage.CHOCOLATE, true, 1, 1);
//
//		//When
//		String message = order.sendOrder();
//
//		//Then
//		assertThat(message).isEqualTo("Hh:1:0");
//	}
}
